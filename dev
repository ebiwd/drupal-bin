#!/bin/bash

# halt on errors
set -e;
set -u;

# determine root of repo
ROOT=$(cd $(dirname ${0})/.. 2>/dev/null && pwd -P);
cd ${ROOT};
# set environment variables
set -a; source ${ROOT}/.env; set +a;

# set some variables to define custom content for this repo
ASSET_TYPES="modules themes profiles";
CUSTOM_FOLDER="custom";
DRUPAL_VER=${DRUPAL_VER:-7};

function usage {
  echo "Local development docker utilities";
  echo "";
  echo "Usage: bin/dev <function> [parameters]";
  echo "";
  echo "  Function is one of:";
  echo "    up                 - spin up docker container for local development";
  echo "    down               - spin down docker container for local development";
  echo "";
  echo "    launch [url]       - open browser for local development";
  echo "    login              - open browser and login ";
  echo "    pma|solr|mailhog   - open local development utilities";
  echo "";
  echo "    logs [container]   - view logs from docker container";
  echo "      optional [container] is one (or more) of apache|php|mysql|pma|mailhog|memcached";
  echo "";
  echo "    ssh [command]";
  echo "      optional [command] can be shell command to run, otherwise will be interactive shell";
  echo "";
  echo "    drush <command>";
  echo "      <command> is drush command to run";
  echo "    cc [options]       - clear caches";
  echo "    rr <options>       - install and run registry_rebuild";
  echo "    dl [project]       - installs project in sites/all/modules/dev";
  echo "    updb <options>     - run database updates";
  echo "    cron <options>     - run cron";
  echo "    en|dis [module]    - enables/disables module";
  echo ""
  echo "    make               - build codebase from makefiles in ${LOCAL_MAKE_PATH}";
  echo "    make-update        - update makefiles with latest versions of modules"
  echo "    make-lock          - update makefiles with versions resolved"
  echo ""
  echo "    install            - perform site install via drush";
  echo "    create-aliases     - create alias file for dev|stage|prod and copy to ~/.drushrc";
  echo "";
  echo "    solr-init          - initialise solr environment and re-index";
  echo "    xhprof-init        - enable drupal modules for profiling";
  echo "";
  echo "    quick <env>        - clone sql and files from dev|stage|prod, spin up docker,";
  echo "                           initialise solr (if enabled), login in browser";
  echo "";
  exit 1;
}

# check for example content in .env
[ ${DRUPAL_SITE} = "example.com" ] && echo "Please edit .env before continuing" && exit 1;

# check system requirements
if ! which docker-compose >/dev/null 2>&1; then
  echo "This script requires docker-compose installed on the local machine";
fi;

if ! drush --version >/dev/null 2>&1; then
  echo "This script requires drush installed on the local machine";
fi;

# make some folders
mkdir -p ${LOCAL_SQLDATA_SRC} ${LOCAL_SQLDUMP_SRC} ${LOCAL_FILES_SRC} ${LOCAL_TEMP_SRC};

# validate the command
[ -z "${1:-}" ] && usage;

# get the command and run it
COMMAND="${1}"; shift;
case "${COMMAND}" in
  quick)
    ${ROOT}/bin/dev create-aliases;
    ${ROOT}/bin/dump sql $*;
    ${ROOT}/bin/dump files $*;
    ${ROOT}/bin/dev up;
    if docker-compose ps solr 2>/dev/null | grep -Fq " Up "; then
      ${ROOT}/bin/dev solr-init
    fi;
    ${ROOT}/bin/dev login;
    ;;

  # spin up docker containers
  up)
    docker-compose up -d --remove-orphans --force-recreate;

    # create reverse symlink to files directory
    ${ROOT}/bin/dev ssh "mkdir -p /var/www/drupal/files && ln -sfv ${DOCKER_FILES_PATH} /var/www/drupal/files/${DRUPAL_SITE}";
    # create legacy html links for static paths defined in viewsxml
    if [ ${DEPLOY_CORE_NAME} != html ]; then
      ${ROOT}/bin/dev ssh "mkdir -p /var/www/drupal/html/sites/${DRUPAL_SITE} && ln -sfv ${DOCKER_FILES_PATH} /var/www/drupal/html/sites/${DRUPAL_SITE}";
    fi;

    # ensure drush up to date
    ${ROOT}/bin/dev ssh 'su-exec www-data composer global require drush/drush:^8';

    # write settings and sites files
    ${ROOT}/bin/dev create-settings;

    # wait for db to import
    ${ROOT}/bin/dev db-wait;

    ${ROOT}/bin/dev launch-advice;
    ;;

  # spin down docker containers
  down)
    docker-compose down -v --remove-orphans;
    docker-compose kill;

    ${ROOT}/bin/dev destroy-settings;
    ;;

  # launch container admin sites
  pma|solr|mailhog)
    DOCKER_PORT=$(docker-compose port traefik 80 | cut -d: -f2);
    ${ROOT}/bin/dev launch http://${COMMAND}.${DOCKER_URL}:${DOCKER_PORT};
    ;;

  # launch site, with one-time login
  login)
    echo "Getting one-time-login url...";
    URL=$(${ROOT}/bin/dev drush user-login 1 '/' | grep -F "${DRUPAL_SITE}");
    ${ROOT}/bin/dev launch ${URL%?};
    ;;

  # open browser
  launch)
    if [ -d "/Applications/Google Chrome.app" ]; then
      # force chrome as browser, as this does not need adjustment in /etc/hosts
      BROWSER="-a/Applications/Google Chrome.app";
    else
      BROWSER="";
      # insert domain into /etc/hosts
      if ! grep -Fq "${DOCKER_URL}" /etc/hosts; then
        echo "Adding ${DOCKER_URL} to /etc/hosts";
        sudo $SHELL -c "echo '127.0.0.1 ${DOCKER_URL} # added $(date) by ${ROOT}/bin/dev' >> /etc/hosts";
      fi;
    fi;

    if [ -z "${1:-}" ]; then
      DOCKER_PORT=$(docker-compose port traefik 80 | cut -d: -f2);
      open "${BROWSER}" http://${DOCKER_URL}:${DOCKER_PORT};
    else
      open "${BROWSER}" ${1};
    fi;
    ;;

  # open ssh-like shell to docker container
  bash | ssh)
    if [ -z "${1:-}" ]; then
      docker-compose exec php bash -c "cd ${DOCKER_CORE_PATH}; exec bash";
    else
      docker-compose exec php bash -c "cd ${DOCKER_CORE_PATH}; $*";
    fi;
    ;;

  # run drush command on docker container
  drush)
    DOCKER_PORT=$(docker-compose port traefik 80 | cut -d: -f2);
    ${ROOT}/bin/dev ssh "env COLUMNS=80 drush --root=${DOCKER_CORE_PATH} --uri=${DOCKER_URL}:${DOCKER_PORT} $*";
    ;;

  cc)
    ${ROOT}/bin/dev drush ${COMMAND} "$*";
    if docker-compose ps memcached 2>/dev/null | grep -Fq " Up "; then
      docker-compose restart memcached;
    fi;
    ;;

  rr)
    ${ROOT}/bin/dev ssh "[ ! -d /root/.drush/registry_rebuild ] && env COLUMNS=80 drush dl registry_rebuild-7.x-2.x-dev && env COLUMNS=80 drush cc drush || true";
    ${ROOT}/bin/dev drush rr "$*";
    ;;

  updb | cron | en | dis)
    ${ROOT}/bin/dev drush ${COMMAND} "$*";
    ;;

  dl)
    ${ROOT}/bin/dev drush ${COMMAND} --destination=sites/all/modules/dev "$*";
    ;;

  # view logs on docker container
  logs)
    docker-compose logs -f $*;
    ;;

  # create .gitignore from example.gitignore (D8)
  create-gitignore)
    # rename example.gitignore to .gitignore
    if [ -f ${LOCAL_CORE_PATH}/example.gitignore ] && [ ! -f ${LOCAL_CORE_PATH}/.gitignore ];
    then
      cp -pv ${LOCAL_CORE_PATH}/example.gitignore ${LOCAL_CORE_PATH}/.gitignore;
    fi;
    ;;

  # build vendor directory from composer.json (D8)
  create-vendor)
    find ${LOCAL_CORE_PATH} -name composer.json ! -path "*/vendor/*" -execdir composer install \;;
    ;;

  # ensure volumes are mounted
  mount-volumes)
    if docker-compose ps php 2>/dev/null | grep -Fq " Up "; then
      docker-compose restart php;
      docker-compose restart memcached;
      ${ROOT}/bin/dev create-settings;
    fi;
    ;;

  # build src from makefiles
  remake)
    # check for conflict in makefile
    if grep -qrE "[^#]\s*subdir:\s+${CUSTOM_FOLDER}" ${LOCAL_MAKE_PATH}; then
      echo "'subdir: ${CUSTOM_FOLDER}' not allowed in ${LOCAL_MAKE_PATH}";
      exit;
    fi;

    # backup custom code
    for ASSET_TYPE in ${ASSET_TYPES}; do
      if [ -d ${LOCAL_SITE_PATH}/${ASSET_TYPE}/${CUSTOM_FOLDER} ]; then
        mkdir -p ${LOCAL_CUSTOM_SRC};
        rm -rf ${LOCAL_CUSTOM_SRC}/${ASSET_TYPE};
        mv -v ${LOCAL_SITE_PATH}/${ASSET_TYPE}/${CUSTOM_FOLDER} ${LOCAL_CUSTOM_SRC}/${ASSET_TYPE};
      fi;
    done;

    # remove old build
    if [ -d ${LOCAL_CORE_PATH} ]; then
      chmod -R u+w ${LOCAL_CORE_PATH};
      rm -rf ${LOCAL_CORE_PATH};
    fi;

    # build core make file
    drush make --yes --force-gitinfofile ${LOCAL_MAKE_PATH}/core.make.yml ${LOCAL_CORE_PATH};
    # build site make file
    if [ -f ${LOCAL_MAKE_PATH}/site.make.yml ]; then
      drush make --yes --no-core --force-gitinfofile ${LOCAL_MAKE_PATH}/site.make.yml --contrib-destination=${LOCAL_SITE_PATH};
    fi;

    # restore custom modules
    for ASSET_TYPE in ${ASSET_TYPES}; do
      if [ -d ${LOCAL_CUSTOM_SRC}/${ASSET_TYPE} ]; then
        mkdir -p ${LOCAL_SITE_PATH}/${ASSET_TYPE};
        mv -v ${LOCAL_CUSTOM_SRC}/${ASSET_TYPE} ${LOCAL_SITE_PATH}/${ASSET_TYPE}/${CUSTOM_FOLDER};
      fi;
    done;

    ${ROOT}/bin/dev create-gitignore;

    ${ROOT}/bin/dev create-vendor;

    echo "Built: $(date)" > ${LOCAL_CORE_PATH}/.build;

    ${ROOT}/bin/dev mount-volumes;
    ;;

  # build src from makefiles, if makefiles are newer
  make)
    # check if makefiles newer the .build file
    if [ ! -f ${LOCAL_CORE_PATH}/.build ] ||
      [ ${LOCAL_MAKE_PATH}/$(ls -1t ${LOCAL_MAKE_PATH} | head -n1) -nt ${LOCAL_CORE_PATH}/.build ];
    then
      ${ROOT}/bin/dev remake;
    else
      # message for local development
      echo "${LOCAL_CORE_PATH} is already built from ${LOCAL_MAKE_PATH}, use 'bin/dev remake' to force make";
    fi;

    # check if files in dist are newer than .build
    # ignore the following:
    SITES_PHP_REGEX="${LOCAL_CORE_PATH/./\.}/sites/sites\.php";
    SETTINGS_PHP_REGEX="${LOCAL_SITE_PATH/./\.}/settings\.php";
    CUSTOM_PHP_REGEX="${LOCAL_SITE_PATH/./\.}/(${ASSET_TYPES/ /|})/${CUSTOM_FOLDER/./\.}";
    if find dist -type f -newer dist/.build | grep -vE "(${SITES_PHP_REGEX}|${SETTINGS_PHP_REGEX}|${CUSTOM_PHP_REGEX})"; then
      # message for local development
      echo "Found files in ${LOCAL_CORE_PATH} that have been modified. Use 'bin/dev remake' to force make";
      exit 1;
    fi;

    ${ROOT}/bin/dev create-vendor;

  ;;

  # run drush site install
  install)
    ${ROOT}/bin/dev drush site-install --sites-subdir=${DEPLOY_SITE_NAME} --account-name=admin --account-pass=admin standard;
    echo "Database initialised with user=admin, pass=admin";
    ${ROOT}/bin/dev launch-advice;
    ;;

  # create aliases file
  create-aliases)
    if [ -d ${LOCAL_ALIASES_SRC} ]; then
      rm -rf ${LOCAL_ALIASES_SRC}/*.php;
    fi;

    mkdir -p ${LOCAL_ALIASES_SRC};

    LOCAL_ALIASES_FILE=${LOCAL_ALIASES_SRC}/${DRUPAL_SITE}.aliases.drushrc.php;
    echo "Creating ${LOCAL_ALIASES_FILE}";

    echo "<?php" > ${LOCAL_ALIASES_FILE};
    for ENVIRONMENT in dev stage prod; do
      case "${ENVIRONMENT}" in
        dev) REMOTE_HOST=${DEV_SERVER%% *} ;;
        stage) REMOTE_HOST=${STAGE_SERVER%% *} ;;
        prod) REMOTE_HOST=${PROD_SERVER%% *} ;;
      esac;
      echo "
        \$aliases['$ENVIRONMENT'] = array(
          'uri' => 'http://${ENVIRONMENT}.${DEPLOY_INSTANCE}.${DRUPAL_SITE}',
          'root' => '${VM_CORE_PATH}',
          'remote-host' => '${REMOTE_HOST}',
          'remote-user' => '${SSH_OWNER}',
          'path-aliases' => array(
            '%drush-script' => '/nfs/public/rw/homes/${SSH_OWNER}/bin/drush',
            '%files' => '${VM_FILES_SRC}',
            '%temp' => '${VM_TEMP_PATH}',
          ),
        );" >> ${LOCAL_ALIASES_FILE};
    done;
    mkdir -p ~/.drush;
    cp -v ${LOCAL_ALIASES_FILE} ~/.drush/;

    echo "";
    echo "To use aliases to connect to drupal on VM, use";
    echo "  drush @${DRUPAL_SITE}.(dev|stage|prod) <command>";
    ;;

  # internal only function to display information about site
  launch-advice)
    DOCKER_PORT=$(docker-compose port traefik 80 | cut -d: -f2);
    echo;
    echo "Open site with";
    echo "  bin/dev launch";
    echo "or visit";
    echo "  http://${DOCKER_URL}:${DOCKER_PORT}";
    echo;
    echo "Open and login with";
    echo "  bin/dev login";
    echo;
    ;;

  # internal only function to wait for db import for complete
  db-wait)
    if [ -f ${LOCAL_SQLDUMP_SRC}/* ] && [ ! -d ${LOCAL_SQLDATA_SRC}/${DOCKER_DATABASE} ]; then
      echo "Please wait for database to import...";
      # wait for drupal database to start importing
      echo "Waiting for ${LOCAL_SQLDATA_SRC}/${DOCKER_DATABASE} to be created";
      while [ ! -d ${LOCAL_SQLDATA_SRC}/${DOCKER_DATABASE} ]; do sleep 1; done;
      # wait for database to restart
      PID=$(ls ${LOCAL_SQLDATA_SRC}/*.pid);
      echo "Waiting for ${PID} to end";
      while [ -f ${PID} ]; do echo -n ''; done;
      # wait for reboot
      echo -n "Waiting for new ${LOCAL_SQLDATA_SRC}/*.pid";
      while [ ! -f ${LOCAL_SQLDATA_SRC}/*.pid ]; do sleep 1; done;
      PID=$(ls ${LOCAL_SQLDATA_SRC}/*.pid);
      echo " - ${PID} started";
    fi;
    ;;

  # internal only function to create settings files
  create-settings)
    DOCKER_PORT=$(docker-compose port traefik 80 | cut -d: -f2);

    # sanity checks
    if [ ! -d ${LOCAL_CORE_PATH} ]; then
      echo "No code in ${LOCAL_CORE_PATH}";
      exit 1;
    fi;

    # write settings file
    mkdir -p ${LOCAL_SITE_PATH};

    if [ ! -f ${LOCAL_SITE_PATH}/settings.php ]; then
      echo "Creating ${LOCAL_SITE_PATH}/settings.php";
      cp -vp ${LOCAL_CORE_PATH}/sites/default/default.settings.php ${LOCAL_SITE_PATH}/settings.php;
      echo "
        \$settings['hash_salt'] = '1234567890';
      " >> ${LOCAL_SITE_PATH}/settings.php

      echo "  Adding database settings";
      if [ ${DRUPAL_VER} -eq "6" ]; then
        echo "
          \$db_url = 'mysqli://${DOCKER_DATABASE_USER}:${DOCKER_DATABASE_PASS}@mysql/${DOCKER_DATABASE}';
          \$db_prefix = '';
        " >> ${LOCAL_SITE_PATH}/settings.php;
      else
        echo "
          \$databases['default']['default'] = array(
            'driver' => 'mysql', 'prefix' => '',
            'host' => 'mysql',
            'database' => '${DOCKER_DATABASE}', 'username' => '${DOCKER_DATABASE_USER}', 'password' => '${DOCKER_DATABASE_PASS}',
          );
        " >> ${LOCAL_SITE_PATH}/settings.php;
      fi;

      echo "  Adding file paths";
      if [ ${DRUPAL_VER} -eq "6" ]; then
        echo "
          \$conf['file_directory_path'] = '${DOCKER_FILES_PATH#${DOCKER_CORE_PATH}/}';
          \$conf['file_directory_temp'] = '${DOCKER_TEMP_PATH}';
        " >> ${LOCAL_SITE_PATH}/settings.php;
      else
        echo "
          \$conf['file_public_path'] = '${DOCKER_FILES_PATH#${DOCKER_CORE_PATH}/}';
          \$conf['file_private_path'] = '${DOCKER_FILES_PATH#${DOCKER_CORE_PATH}/}/private';
          \$conf['file_temporary_path'] = '${DOCKER_TEMP_PATH}';
        " >> ${LOCAL_SITE_PATH}/settings.php;
      fi;

      echo "
        \$conf['drupal_http_request_fails'] = FALSE;
      " >> ${LOCAL_SITE_PATH}/settings.php;

      if docker-compose ps memcached 2>/dev/null | grep -Fq " Up " && [ -f ${LOCAL_CORE_PATH}/sites/all/modules/contrib/memcache/memcache.inc ]; then
        echo "  Adding memcache settings";
        if [ ${DRUPAL_VER} -eq "6" ]; then
          echo "
            \$conf['cache_inc'] = 'sites/all/modules/contrib/memcache/memcache.inc';
          " >> ${LOCAL_SITE_PATH}/settings.php;
        else
          echo "
            \$conf['cache_backends'][] = 'sites/all/modules/contrib/memcache/memcache.inc';
          " >> ${LOCAL_SITE_PATH}/settings.php;
        fi;
        echo "
          \$conf['cache_default_class'] = 'MemCacheDrupal';
          \$conf['memcache_key_prefix'] = '${DRUPAL_SITE//./_}';
          \$conf['memcache_servers'] = array ('memcached:11211' => 'default');
          \$conf['memcache_bins'] = array('cache' => 'default');
        " >> ${LOCAL_SITE_PATH}/settings.php;
      fi;
    fi;

    # write sites file
    if [ ! -f ${LOCAL_CORE_PATH}/sites/sites.php ]; then
      echo "Creating ${LOCAL_CORE_PATH}/sites/sites.php";
      if [ ${DRUPAL_VER} -eq "6" ]; then
        ln -s ${DEPLOY_SITE_NAME} ${LOCAL_CORE_PATH}/sites/${DOCKER_PORT}.${DRUPAL_SITE}.docker.localhost;
        ln -s ${DEPLOY_SITE_NAME} ${LOCAL_CORE_PATH}/sites/${DRUPAL_SITE}.docker.localhost;
      else
        echo "<?php
          \$sites['${DOCKER_PORT}.${DRUPAL_SITE}.docker.localhost'] = '${DEPLOY_SITE_NAME}';
          \$sites['${DRUPAL_SITE}.docker.localhost'] = '${DEPLOY_SITE_NAME}';
        " >> ${LOCAL_CORE_PATH}/sites/sites.php;
      fi;
    fi;
    ;;

  # internal only function to destroy settings files
  destroy-settings)
    # reset permissions of settings and sites files as drupal changes them for security
    chmod -R u+w ${LOCAL_SITE_PATH} || true;
    # remove settings and sites files
    echo "Removing settings files:"
    if [ -f ${LOCAL_SITE_PATH}/settings.php ]; then
      rm -fv ${LOCAL_SITE_PATH}/settings.php;
    fi;
    if [ -f ${LOCAL_CORE_PATH}/sites/sites.php ]; then
      rm -rv ${LOCAL_CORE_PATH}/sites/sites.php;
    fi;
    find dist/sites -type l -name "*.docker.localhost" -delete;
    ;;

  solr-init)
    # create solr core
    if docker-compose ps solr 2>/dev/null | grep -Fq " Up "; then
      echo "Creating solr core";
      docker-compose exec solr ping core=${DRUPAL_SITE} -f /usr/local/bin/actions.mk >/dev/null || docker-compose exec solr make core=${DRUPAL_SITE} -f /usr/local/bin/actions.mk;
      echo "Updating settings";
      ${ROOT}/bin/dev drush solr-set-env-url http://solr:8983/solr/${DRUPAL_SITE};
      echo "Rebuilding index";
      ${ROOT}/bin/dev drush solr-delete-index;
      ${ROOT}/bin/dev drush solr-index;
    else
      echo "You can only run ${COMMAND} while the containers are running";
      exit 1;
    fi;
    ;;

  xhprof-init)
    if docker-compose ps php 2>/dev/null | grep -Fq " Up "; then
      echo "Installing drupal module";
      rm -rf ${ROOT}/${LOCAL_CORE_PATH}/sites/all/modules/dev/xhprof || true;
      git clone --branch 7.x-1.x https://git.drupal.org/project/XHProf.git ${ROOT}/${LOCAL_CORE_PATH}/sites/all/modules/dev/xhprof;
      (
        # patch module to store paths
        cd ${ROOT}/${LOCAL_CORE_PATH}/sites/all/modules/dev/xhprof;
        wget https://www.drupal.org/files/issues/xhprof-2354853-paths-d7-4.patch;
        wget https://www.drupal.org/files/issues/2631144-administrative-paths-admin-menu-4.patch;
        wget https://www.drupal.org/files/issues/xhprof-2335669-prevent-segmentation-fault-1.patch;
        wget https://www.drupal.org/files/issues/xhprof-2886366-show-description-1.patch
        git apply -v *.patch;
      )

      # set xhprof variables, unset devel xhprof variables, enable xhprof drupal module
      ${ROOT}/bin/dev drush eval "\"variable_set('xhprof_enabled', 1);variable_set('xhprof_disable_admin_paths', 1);variable_set('xhprof_disable_admin_menu_paths', 1);variable_set('devel_xhprof_enabled', 0);\"";
      ${ROOT}/bin/dev en --yes xhprof || true;

      # clear drupal cache
      ${ROOT}/bin/dev cc --yes all;
    else
      echo "You can only run ${COMMAND} while the containers are running";
      exit 1;
    fi;
    ;;

  make-update)
    ${ROOT}/bin/dev make-lock;

    for FILE in $(ls ${LOCAL_MAKE_PATH}/*.yml); do
      TEMPNAME=$(mktemp);
      if [ $FILE = "${LOCAL_MAKE_PATH}/core.make.yml" ]; then
        drush make-update $* ${FILE} > ${TEMPNAME};
      else
        drush make-update $* --no-core --strict=0 ${FILE} > ${TEMPNAME};
      fi;
      [ $? = 0 ] && cp ${TEMPNAME} ${FILE};
    done;

    echo "Makefiles updated";
    ;;

  make-lock)
    for FILE in $(ls ${LOCAL_MAKE_PATH}/*.yml); do
      TEMPNAME=$(mktemp);
      if [ $FILE = "${LOCAL_MAKE_PATH}/core.make.yml" ]; then
        drush make-lock $* ${FILE} > ${TEMPNAME};
      else
        drush make-lock $* --no-core ${FILE} > ${TEMPNAME};
      fi;
      [ $? = 0 ] && cp ${TEMPNAME} ${FILE};
    done;
    ;;

  *)
    usage
    ;;
esac;

echo "";
